# gpuMultiprocessing

Few functions that simplify running subprocesses distributed over multiple GPUs.
This code does not actually run anything on a GPU, it is just a clever way
how to exploit the functionality of multiprocessing library which assigns a CPU
to a process it wants to run, but here, we map the CPUid to a GPUid and we make
it visible to the subprocess as an environment variable, so the user can work
with that information within the code they try to run. One clear example is
setting `CUDA_VISIBLE_DEVICES=GPUid` env variable to run your code on a specific
GPU with Tensorflow based on the CPUid which is used to run the subprocess.

This package is neither a replacement of multiprocessing library nor does it
implement all the functions that multiprocessing has to offer. This is merely
a convenient hack how to easily run your scripts in parallel on multiple GPUs.

Warning! This is not a package for model parallelism, i.e. you can not run
one model on multiple GPUs in parallel with this. This package is for running
separate processes on separate GPUs. This comes in handy e.g. in hyper-parameter
search when doing deep learning.

## Installation

`pip install git+https://gitlab.com/paloha/gpuMultiprocessing`

Note: Tested only on Ubuntu 16.04, but it should work elsewhere as well.


## Usage example

#### Example of running queue of 4 commands in parallel on 2 GPUs.

The contents of `example-script.py`:
```python
# This is the code we want to be assigned and aware of the GPUid
import os
print('PID: {}, BATCHSIZE: {}, CUDA_VISIBLE_DEVICES: {}'.format(
    os.getpid(), os.environ['BATCHSIZE'], os.environ['CUDA_VISIBLE_DEVICES']))
```

Using the `queue_runner()` to run `example-script.py` 4 times always with
different values stored in the ENV variables.
```python
# Example of running queue of 4 commands in parallel on 2 GPUs
# The number of CPUs must be equal or larger than the numbrer of GPUs!

import gpuMultiprocessing
gpu_id_list = [0,7] # Lets use these two GPUs   
command_queue = ['BATCHSIZE=16 python example-script.py',
                 'BATCHSIZE=32 python example-script.py',
                 'BATCHSIZE=64 python example-script.py',
                 'BATCHSIZ=80 python example-script.py']  # Typo on purpose

gpuMultiprocessing.queue_runner(command_queue, gpu_id_list,
                                env_gpu_name='CUDA_VISIBLE_DEVICES',
                                processes_per_gpu=2, allowed_restarts=1)
```

The output produced (simplified):
```
PID: 23207, BATCHSIZE: 16, CUDA_VISIBLE_DEVICES: 0  # Successful
PID: 23210, BATCHSIZE: 64, CUDA_VISIBLE_DEVICES: 0  # Successful
PID: 23208, BATCHSIZE: 32, CUDA_VISIBLE_DEVICES: 7  # Successful
KeyError: 'BATCHSIZE'  # Failed (because of the typo)
KeyError: 'BATCHSIZE'  # Failed again

# Since there was no successful command run since the last loop
# the restart_counter starts to track number of restarts
Restarting 1 failed commands.
KeyError: 'BATCHSIZE'  # Failed again
Restart counter reached its limit. Some of the commands failed.
DONE. Total time: 0.0054 minutes.

# Returned the queue containing all failed commands
Out[0]: ['BATCHSIZ=80 python example-script.py']  
```
