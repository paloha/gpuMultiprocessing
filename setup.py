from setuptools import setup

setup(
    name='gpuMultiprocessing',
    version='0.0.001',
    author='Pavol Harar',
    author_email='pavol.harar@gmail.com',
    packages=['gpuMultiprocessing'],
    description='Small library of functions which simplify running multiple processes on multiple GPUs.',
    long_description=open('readme.md').read(),
    include_package_data=True,
    install_requires=[],
    license='MIT',
)
